import java.util.ArrayList;
import java.util.List;

import org.ejml.simple.SimpleMatrix;


public class BSPTree {
    // private boolean stop;
    private BinaryTree<Face> tree;

    BSPTree(List<Face> faces) {
        // stop = true;
        // while(stop) {}

        tree = new BinaryTree<>(null);
        tree.root.data = partitionSpace(
            faces.subList(1, faces.size() - 1), tree.root);
    }

    private Face partitionSpace(
        List<Face> faces,
        BinaryTree<Face>.Node curNode
    ) {
        // System.out.printf("Input faces: %d\n", faces.size());
        Face refFace = faces.get(0);
        curNode.data = refFace;
        if(faces.size() == 1) {
            return refFace;
        }

        List<Face> behind, inFront;
        behind = new ArrayList<>();
        inFront = new ArrayList<>();

        for(int i = 1; i < faces.size(); i++) {
            Face testedFace = faces.get(i);
            List<SimpleMatrix> frontVertices, backVertices;
            frontVertices = new ArrayList<>();
            backVertices = new ArrayList<>();

            for(int j = 0; j < testedFace.vertices.length; j++) {
                SimpleMatrix p0 = testedFace.vertices[j];
                SimpleMatrix p1 = testedFace
                    .vertices[(j + 1) % testedFace.vertices.length];

                double p0dist = refFace.applyToPlaneEquation(p0);
                double p1dist = refFace.applyToPlaneEquation(p1);
                if(p0dist > 0) {
                    frontVertices.add(p0);
                    if(p1dist < 0) {
                        SimpleMatrix p01 = getEdgeFaceIntersection(p0, p1, refFace);
                        frontVertices.add(p01);
                        backVertices.add(p01);
                    }
                } else if(p0dist < 0) {
                    backVertices.add(p0);
                    if(p1dist > 0) {
                        SimpleMatrix p01 = getEdgeFaceIntersection(p0, p1, refFace);
                        frontVertices.add(p01);
                        backVertices.add(p01);
                    }
                } else {
                    frontVertices.add(p0);
                    backVertices.add(p0);
                }
            }

            if(frontVertices.size() > 2)
                inFront.add(new Face(frontVertices.toArray(
                    new SimpleMatrix[0])));
            
            if(backVertices.size() > 2)
                behind.add(new Face(backVertices.toArray(
                    new SimpleMatrix[0])));
        }
        if(inFront.size() > 0) {
            curNode.addLeft(null);
            curNode.left().data = partitionSpace(inFront, curNode.left());
        }
        if(behind.size() > 0) {
            curNode.addRight(null);
            curNode.right().data = partitionSpace(behind, curNode.right());
        }

        return refFace;
    }

    private SimpleMatrix getEdgeFaceIntersection(
        SimpleMatrix p0,
        SimpleMatrix p1,
        Face face
    ) {
        SimpleMatrix lv = p1.minus(p0);
        SimpleMatrix fp = face.vertices[0];
        double d = Transformations.dotProduct(fp.minus(p0), face.normal) /
            Transformations.dotProduct(lv, face.normal);
        return p0.plus(lv.scale(d));
    }

    public List<Face> getFacesInVisibilityOrder(SimpleMatrix viewPoint) {
        List<Face> l = new ArrayList<>();
        l.addAll(traverse(tree.root, viewPoint));

        // System.out.printf(
        //     "Faces: %d\n", l.size()
        // );
        // tree.printTree();

        return l;
    }

    private List<Face> traverse(
        BinaryTree<Face>.Node node,
        SimpleMatrix viewPoint
    ) {
        List<Face> list = new ArrayList<>();
        double d = node.data.applyToPlaneEquation(viewPoint);

        if(d >= 0) {
            if(node.right() != null)
                list.addAll(traverse(node.right(), viewPoint));
            list.add(node.data);
            if(node.left() != null)
                list.addAll(traverse(node.left(), viewPoint));
        } else {
            if(node.left() != null)
                list.addAll(traverse(node.left(), viewPoint));
            list.add(node.data);
            if(node.right() != null)
                list.addAll(traverse(node.right(), viewPoint));
        }

        return list;
    }
}