import org.ejml.simple.SimpleMatrix;

public class Transformations {

    private Transformations() {
    }

    public static SimpleMatrix transformLocalToWorld(
        SimpleMatrix p, SimpleMatrix position) {
        return (
            getTranslationMatrix(position)
            .mult(p));
    }

    public static SimpleMatrix transformWorldToCamera(
        SimpleMatrix p, SimpleMatrix position, SimpleMatrix rotation
        ) {
        SimpleMatrix antiPosition = new SimpleMatrix(new double[][]{
            {
                -position.get(0),
                -position.get(1),
                -position.get(2),
                position.get(3)
            }
        }).transpose();
        SimpleMatrix antiRotation = new SimpleMatrix(new double[][]{
            {
                -rotation.get(0),
                -rotation.get(1),
                -rotation.get(2),
                rotation.get(3)
            }
        }).transpose();
        return (
            getXRotationMatrix(antiRotation)
            .mult(getYRotationMatrix(antiRotation))
            .mult(getTranslationMatrix(antiPosition))
            .mult(p));
    }

    public static SimpleMatrix transformCameraToScreen(
        SimpleMatrix p, Camera camera, double WINDOW_WIDTH,
        double WINDOW_HEIGHT) {
        return (
            getScreenTranslationMatrix(WINDOW_WIDTH, WINDOW_HEIGHT)
            .mult(getScreenScaleMatrix(WINDOW_WIDTH, WINDOW_HEIGHT))
            .mult(getYOZMirrorMatrix())
            .mult(normalizeVector(getProjectionMatrix(camera.fov)
                .mult(p))));
    }

    public static SimpleMatrix getTranslationMatrix(SimpleMatrix v) {
        return new SimpleMatrix(new double[][] {
            {1.0, 0.0, 0.0, v.get(0)},
            {0.0, 1.0, 0.0, v.get(1)},
            {0.0, 0.0, 1.0, v.get(2)},
            {0.0, 0.0, 0.0, 1.0}
        });
    }

    public static SimpleMatrix getYRotationMatrix(SimpleMatrix v) {
        return new SimpleMatrix(new double[][] {
            {Math.cos(v.get(1)), 0.0, Math.sin(v.get(1)), 0.0},
            {0.0, 1.0, 0.0, 0.0},
            {-Math.sin(v.get(1)), 0.0, Math.cos(v.get(1)), 0.0},
            {0.0, 0.0, 0.0, 1.0}
        });
    }

    public static SimpleMatrix getXRotationMatrix(SimpleMatrix v) {
        return new SimpleMatrix(new double[][] {
            {1.0, 0.0, 0.0, 0.0},
            {0.0, Math.cos(v.get(0)), -Math.sin(v.get(0)), 0.0},
            {0.0, Math.sin(v.get(0)), Math.cos(v.get(0)), 0.0},
            {0.0, 0.0, 0.0, 1.0}
        });
    }

    public static SimpleMatrix getYOZMirrorMatrix() {
        return new SimpleMatrix(new double[][] {
            {1.0, 0.0, 0.0, 0.0},
            {0.0, -1.0, 0.0, 0.0},
            {0.0, 0.0, 1.0, 0.0},
            {0.0, 0.0, 0.0, 1.0}
        });
    }

    public static SimpleMatrix getScreenScaleMatrix(double WINDOW_WIDTH,
        double WINDOW_HEIGHT) {
        return new SimpleMatrix(new double[][] {
            {WINDOW_WIDTH / 2.0, 0.0, 0.0, 0.0},
            {0.0, WINDOW_HEIGHT / 2.0, 0.0, 0.0},
            {0.0, 0.0, 1.0, 0.0},
            {0.0, 0.0, 0.0, 1.0}
        });
    }

    public static SimpleMatrix getProjectionMatrix(double fov) {
        double n = 0.1, f = 10.0;
        double s = 1.0 / Math.tan(Math.toRadians(fov / 2.0));
        return new SimpleMatrix(new double[][] {
            {s, 0.0, 0.0, 0.0},
            {0.0, s, 0.0, 0.0},
            {0.0, 0.0, -(f+n)/(f-n), -1.0},
            {0.0, 0.0, -n*f/(f-n), 0.0}
        }).transpose();
    }

    public static SimpleMatrix normalizeVector(SimpleMatrix m) {
        SimpleMatrix mat = new SimpleMatrix(new double[][]{
            {1.0/m.get(3), 0, 0, 0},
            {0, 1.0/m.get(3), 0, 0},
            {0, 0, 1.0/m.get(3), 0},
            {0, 0, 0, 1.0/m.get(3)}
        });
        return mat.mult(m);
    }

    public static SimpleMatrix getScreenTranslationMatrix(
        double WINDOW_WIDTH, double WINDOW_HEIGHT
    ) {
        return getTranslationMatrix(new SimpleMatrix(new double[][] {
            {WINDOW_WIDTH / 2.0, WINDOW_HEIGHT / 2.0, 0.0, 1.0}
        }).transpose());
    }

    public static SimpleMatrix crossProduct(SimpleMatrix a, SimpleMatrix b) {
        return new SimpleMatrix(new double[][]{
            {a.get(1)*b.get(2) - a.get(2)*b.get(1)},
            {a.get(2)*b.get(0) - a.get(0)*b.get(2)},
            {a.get(0)*b.get(1) - a.get(1)*b.get(0)},
            {1.0}
        });
    }

    public static double dotProduct(SimpleMatrix a, SimpleMatrix b) {
        return a.get(0)*b.get(0) + a.get(1)*b.get(1) + a.get(2)*b.get(2);
    }
}