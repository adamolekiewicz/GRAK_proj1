import java.util.ArrayList;
import java.util.List;

import org.ejml.simple.SimpleMatrix;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 * Main
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch();
    }

    private GraphicsContext context;
    private final double WINDOW_WIDTH = 800.0, WINDOW_HEIGHT = 600.0;
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        Canvas canvas = new Canvas(WINDOW_WIDTH, WINDOW_HEIGHT);
        Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT);
        context = canvas.getGraphicsContext2D();

        context.setFill(Color.BLACK);
        context.setStroke(Color.WHITE);
        context.fillRect(0, 0, 800, 600);
        context.setTextAlign(TextAlignment.LEFT);
        context.setTextBaseline(VPos.TOP);

        scene.addEventHandler(KeyEvent.KEY_PRESSED, keyPressHandler);
        scene.addEventHandler(KeyEvent.KEY_RELEASED, keyReleaseHandler);
        root.getChildren().add(canvas);

        primaryStage.setScene(scene);

        primaryStage.show();

        setupScene();
        AnimationTimer timer = new AnimationTimer(){
            @Override
            public void handle(long now) {
                updateScene();
                drawScene();
            }
        };
        timer.start();
    }
    
    private Camera camera;
    private Mesh[] meshes;
    private BSPTree bsptree;

    private void setupScene() {
        camera = new Camera();
        meshes = new Mesh[] {
            new Mesh(0.4, 0.4, 0.4),
            new Mesh(0.4, 0.8, 0.4),
            new Mesh(0.8, 0.8, 0.8),
            new Mesh(0.4, 0.2, 0.4)
        };
        meshes[0].setPosition(new SimpleMatrix(new double[][]{
            {-.8, 0.0, .8, 1.0}
        }).transpose());
        meshes[1].setPosition(new SimpleMatrix(new double[][]{
            {-.8, 0.0, 1.4, 1.0}
        }).transpose());
        meshes[2].setPosition(new SimpleMatrix(new double[][]{
            {0.0, 0.0, .8, 1.0}
        }).transpose());
        meshes[3].setPosition(new SimpleMatrix(new double[][]{
            {0.0, 0.0, 1.8, 1.0}
        }).transpose());

        List<Face> faces = new ArrayList<>();
        for(Mesh m : meshes) {
            for(Face f : m.defaultFaces) {
                SimpleMatrix[] verticesWorld = new SimpleMatrix[f.vertices.length];
                for(int i = 0; i < f.vertices.length; i++) {
                    verticesWorld[i] = Transformations
                        .transformLocalToWorld(f.vertices[i], m.position);
                }
                faces.add(new Face(verticesWorld));
            }
        }

        bsptree = new BSPTree(faces);
    }

    private void updateScene() {
        camera.rotation = camera.rotation.plus(camera.rotationSpeed);
        camera.position = camera.position.plus(
            Transformations.getYRotationMatrix(camera.rotation).mult(camera.speed));
        double newFov = camera.fov + camera.zoomSpeed;
        camera.fov = newFov > 120 ? 120 : newFov < 30 ? 30 : newFov;
    }

    private final SimpleMatrix cameraForwardVector = new SimpleMatrix(new double[][]{
        {0.0},
        {0.0},
        {-1.0},
        {1.0}
    });
    private void drawScene() {
        context.setFill(Color.BLACK);
        context.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

        List<Face> facesWorld = bsptree.getFacesInVisibilityOrder(camera.position);
        System.out.println(facesWorld.toString());
        for(Face f : facesWorld) {
            SimpleMatrix[] verticesCamera = new SimpleMatrix[f.vertices.length];
            SimpleMatrix[] verticesScreen = new SimpleMatrix[f.vertices.length];

            for(int i = 0; i < f.vertices.length; i++) {
                verticesCamera[i] = Transformations.transformWorldToCamera(
                    f.vertices[i], camera.position, camera.rotation);
                verticesScreen[i] = Transformations.transformCameraToScreen(
                    verticesCamera[i], camera, WINDOW_WIDTH, WINDOW_HEIGHT);
                
                // verticesScreen[i].set(2, Math.max(0, verticesScreen[i].get(2)));

                // Debug info    
                // context.strokeText(
                //     // String.format("%.4f", verticesCamera[i].get(2)),
                //     verticesCamera[i].toString(),
                //     verticesScreen[i].get(0),
                //     verticesScreen[i].get(1));
            }

            context.setFill(f.color);
            context.beginPath();
            context.moveTo(
                verticesScreen[verticesScreen.length - 1].get(0),
                verticesScreen[verticesScreen.length - 1].get(1));
            for(int i = 0; i < verticesScreen.length; i++) {
                if(verticesCamera[i].get(2) <= 0) {
                    context.lineTo(verticesScreen[i].get(0),
                        verticesScreen[i].get(1));
                }
            }
            context.closePath();
            context.fill();
        }

        // Further debug info
        // SimpleMatrix cameraPivot = transformCameraToScreen(
        //     new SimpleMatrix(new double[][] {
        //         {0, 0, 0, 1}
        //     }).transpose());
        // context.getPixelWriter().setColor(
        //     (int)cameraPivot.get(0), (int)cameraPivot.get(1), Color.YELLOW);
        // context.strokeText(camera.position.toString(), 0, 0);
        // context.strokeText(camera.rotation.toString(), 200, 0);
    }

    private final double CAM_SPEED = 0.05, CAM_ROT_SPEED = 0.02,
        CAM_ZOOM_SPEED = 1; 
    private final EventHandler<KeyEvent> keyPressHandler = new EventHandler<>() {
        @Override
        public void handle(KeyEvent event) {
            switch (event.getCode()) {
            case W:
                camera.speed.set(2, -CAM_SPEED);
                // Inverted due to Z axis flip during projection
                break;
            case S:
                camera.speed.set(2, CAM_SPEED);
                break;
            case A:
                camera.speed.set(0, -CAM_SPEED);
                break;
            case D:
                camera.speed.set(0, CAM_SPEED);
                break;
            case Q:
                camera.speed.set(1, CAM_SPEED);
                break;
            case E:
                camera.speed.set(1, -CAM_SPEED);
                break;
            case UP:
                camera.rotationSpeed.set(0, -CAM_ROT_SPEED);
                // Inverted due to author's preference
                break;
            case DOWN:
                camera.rotationSpeed.set(0, CAM_ROT_SPEED);
                break;
            case LEFT:
                camera.rotationSpeed.set(1, CAM_ROT_SPEED);
                break;
            case RIGHT:
                camera.rotationSpeed.set(1, -CAM_ROT_SPEED);
                break;
            case PAGE_UP:
                camera.zoomSpeed = CAM_ZOOM_SPEED;
                break;
            case PAGE_DOWN:
                camera.zoomSpeed = -CAM_ZOOM_SPEED;
                break;
            default:
                break;
            }
        }
    };

    private EventHandler<KeyEvent> keyReleaseHandler = new EventHandler<>() {
        @Override
        public void handle(KeyEvent event) {
            switch (event.getCode()) {
            case W:
                // camera.setVForward(0);
                camera.speed.set(2, 0);
                break;
            case S:
                // camera.setVForward(0);
                camera.speed.set(2, 0);
                break;
            case A:
                // camera.setVRight(0);
                camera.speed.set(0, 0);
                break;
            case D:
                // camera.setVRight(0);
                camera.speed.set(0, 0);
                break;
            case Q:
                // camera.setVUp(0);
                camera.speed.set(1, 0);
                break;
            case E:
                // camera.setVUp(0);
                camera.speed.set(1, 0);
                break;
            case UP:
                // camera.setVRotX(0);
                camera.rotationSpeed.set(0, 0);
                break;
            case DOWN:
                // camera.setVRotX(0);
                camera.rotationSpeed.set(0, 0);
                break;
            case LEFT:
                // camera.setVRotY(0);
                camera.rotationSpeed.set(1, 0);
                break;
            case RIGHT:
                // camera.setVRotY(0);
                camera.rotationSpeed.set(1, 0);
                break;
            case PAGE_UP:
                camera.zoomSpeed = 0;
                break;
            case PAGE_DOWN:
                camera.zoomSpeed = 0;
                break;
            default:
                break;
            }
        }
    };

}