import java.util.Random;

import org.ejml.simple.SimpleMatrix;

import javafx.scene.paint.Color;

public class Face {
    public Color color;
    public SimpleMatrix[] vertices;
    public SimpleMatrix normal;
    public double A, B, C, D;

    Face(SimpleMatrix[] vs) {
        Random random = new Random();
        color = new Color(random.nextDouble(), random.nextDouble(),
            random.nextDouble(), 1.0);
        vertices = vs;
        createPlaneEquation();
    }

    Face(Face f) {
        color = f.color;
        vertices = f.vertices;
        A = f.A;
        B = f.B;
        C = f.C;
        D = f.D;
    }

    public double applyToPlaneEquation(SimpleMatrix v) {
        return A*v.get(0) + B*v.get(1) + C*v.get(2) + D;
    }

    private void createPlaneEquation() {
        SimpleMatrix v1, v2;
        v1 = vertices[1].minus(vertices[0]);
        v2 = vertices[2].minus(vertices[0]);
        normal = Transformations.crossProduct(v1, v2);
        A = normal.get(0);
        B = normal.get(1);
        C = normal.get(2);
        D = -Transformations.dotProduct(normal, vertices[0]);
    }
}
