import java.util.Arrays;

import org.ejml.simple.SimpleMatrix;

public class Mesh {
    public final SimpleMatrix[] vertices;
    public final int[][] edges;
    public SimpleMatrix position;
    public final Face[] defaultFaces;

    public Mesh(double sizeX, double sizeY, double sizeZ) {
        double[][] vertices_raw = new double[][] {
            {0.0, 0.0, 0.0, 1.0},
            {0.0, 0.0, sizeZ, 1.0},
            {sizeX, 0.0, sizeZ, 1.0},
            {sizeX, 0.0, 0.0, 1.0},
            {0.0, sizeY, 0.0, 1.0},
            {0.0, sizeY, sizeZ, 1.0},
            {sizeX, sizeY, sizeZ, 1.0},
            {sizeX, sizeY, 0.0, 1.0}
        };
        vertices = Arrays.stream(vertices_raw).map(
            v -> new SimpleMatrix(new double[][]{v}).transpose()
            ).toArray(SimpleMatrix[]::new);
        position = new SimpleMatrix(new double[][]{
            {0.0, 0.0, 0.0, 1.0}
        }).transpose();
        edges = new int[][]{
            {0, 1},
            {1, 2},
            {2, 3},
            {3, 0},
            {4, 5},
            {5, 6},
            {6, 7},
            {7, 4},
            {0, 4},
            {1, 5},
            {2, 6},
            {3, 7}
        };
        SimpleMatrix[] v = vertices;
        defaultFaces = new Face[]{
            new Face(new SimpleMatrix[]{v[0], v[1], v[2], v[3]}),
            new Face(new SimpleMatrix[]{v[0], v[4], v[7], v[3]}),
            new Face(new SimpleMatrix[]{v[1], v[5], v[4], v[0]}),
            new Face(new SimpleMatrix[]{v[2], v[6], v[5], v[1]}),
            new Face(new SimpleMatrix[]{v[3], v[7], v[6], v[2]}),
            new Face(new SimpleMatrix[]{v[4], v[5], v[6], v[7]})
        };
    }

    public void setPosition(SimpleMatrix position) {
        this.position = position;
    }
}