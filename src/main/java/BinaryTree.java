import java.util.ArrayList;
import java.util.List;

public class BinaryTree<T> {
    public Node root;

    public BinaryTree(T rootData) {
        root = new Node();
        root.data = rootData;
    }

    public void printTree() {
        List<T> l = new ArrayList<>();
        l.addAll(root.serialize());
        System.out.println(l.toString());
    }

    // public BinaryTree(Node<T> rootNode) {
    //     root = rootNode;
    // }

    public class Node {
        public T data;
        private Node parent;
        private Node l, r;   // children

        public void addLeft(T data) {
            l = new Node();
            l.parent = this;
            l.data = data;
        }

        public void addRight(T data) {
            r = new Node();
            r.parent = this;
            r.data = data;
        }

        public Node left() {
            return l;
        }

        public Node right() {
            return r;
        }

        public Node parent() {
            return parent;
        }

        public List<T> serialize() {
            List<T> list = new ArrayList<>();
            list.add(data);
            if(l != null) {
                list.addAll(l.serialize());
            }
            if(r != null) {
                list.addAll(r.serialize());
            }
            return list;
        }
    }
}